<?php


namespace App\Repository;


interface NewsDataRepositoryInterface
{

    public function getNewsData();

    public function searchNewsData($term);

    public function newsSingle($slug);

    public function getNewsCategories();

    public function getNewsByCategory($category_slug, $per_page);

    public function getLatestNews();

    public function getNewsCategoryFromSlug($slug);

    public function getEntertainmentNews();

    public function resize_image($request);

    public function saveBreakingNews($data);

    public function sendContactUs($data);

    public function sendSubmitStory($data);

    public function saveSubscribeEmailId($email);

    public function runAutoPost();

    public function getNewsByMonth($date, $per_page);

    public function getArticalMonths();
}
