<?php

namespace App\Http\Controllers;

use App\Models\EmailSubscription;
use App\Models\MainNews;
use App\Repository\NewsDataRepositoryInterface;
use App\Services\Slug;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $newsRepository;

    public function __construct(NewsDataRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.home');
    }

    public function add_breaking_news()
    {
        return view('dashboard.add_breaking_news');
    }

    public function save_breaking_news(Request $request)
    {
        $slug = new Slug();
        $data = array(
            'title' => $request->news_title,
            'short_description' => $request->news_short_description,
            'long_description' => $request->news_long_description,
            'thumb' => $this->newsRepository->resize_image($request),
            'published_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'slug' => $slug = $slug->createSlug($request->news_title),
            'is_fetch_all_info' => 1,
            'source' => 'admin',
            'source_type' => 'Breaking News',
        );
        $this->newsRepository->saveBreakingNews($data);

        return redirect()->back()->with('status', 'Breaking News Added Successfully!');
    }

    public function list_breaking_news()
    {
        $news = MainNews::where('source_type', 'Breaking News')->paginate(15);
        return view('dashboard.list_breaking_news')
            ->with('news', $news);
    }

    public function delete_breaking_news($slug)
    {
        MainNews::where('slug', $slug)->delete();
        return redirect()->back()->with('status', 'Breaking News deleted Successfully!');

    }
    public function email_subscribers(){
        $emails = EmailSubscription::where('status',1)->paginate(15);
        return view('dashboard.list_email_subscribers')
            ->with('emails',$emails);
    }
}
