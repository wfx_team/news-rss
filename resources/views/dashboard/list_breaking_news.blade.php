@extends('layouts.dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                List Breaking News
                <small>Breaking News</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Breaking News</a></li>
                <li class="active">List Breaking News</li>
            </ol>
        </section>
        <section class="content">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
        @endif
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">List Breaking News</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row justify-content-center">
                        <div class="col-md-12">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Short Description</th>
                                    <th scope="col">Long Description</th>
                                    <th scope="col" width="100px">Thumbnail</th>
                                    <th scope="col">Published Date</th>
                                    <th scope="col">slug</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($news as $newsone)
                                    <tr>
                                        <th scope="row">{{$x}}</th>
                                        <td>{{$newsone->title}}</td>
                                        <td>{{$newsone->short_description}}</td>
                                        <td>{{$newsone->long_description}}</td>
                                        <td><img src="{{$newsone->thumb}}" alt="" class="img-thumbnail"></td>
                                        <td>{{$newsone->published_date}}</td>
                                        <td>{{$newsone->slug}}</td>
                                        <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('site.singleView',['slug'=>$newsone->slug])}}"
                                               target="_blank">View</a>
                                            <a class="btn btn-danger btn-sm"
                                               href="{{route('dashboard.delete_breaking_news',['slug'=>$newsone->slug])}}">Delete</a>

                                        </td>
                                    </tr>
                                    @php $x++; @endphp

                                @endforeach
                                </tbody>
                            </table>

                            {{$news->links()}}

                        </div>
                    </div>

                </div>
                <!-- /.box -->

        </section>
        <div class="container">

        </div>
    </div>

@endsection

@section('extra-js')

@endsection
