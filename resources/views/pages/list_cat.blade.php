@extends('layout.main')

@section('content')
    <div class="breadcumb-area section_padding_50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breacumb-content d-flex align-items-center justify-content-between">

                        <div class="gazette-post-tag">
                            <a href="{{route('site.categoryView',['slug'=>$category->slug])}}">{{$category->name}}</a>
                        </div>
                        <p class="editorial-post-date text-dark mb-0">{{$news_data->total()}} Result(s) Found | <b>{{count($news_data)}}</b> Showing</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="catagory-welcome-post-area section_padding_100">
        <div class="container">
            @foreach($news_data->chunk(3) as $chuncked)
                <div class="row">
                    @foreach($chuncked as $data)
                        <div class="col-12 col-md-4">

                            <div class="gazette-welcome-post">

                                <div class="gazette-post-tag">
                                    <a href="{{route('site.categoryView',['slug'=>$data->cat_slug])}}">{{$data->cat_name}}</a>
                                </div>
                                <h2 class="font-pt">{{\Illuminate\Support\Str::limit(strip_tags($data->title),60)}}</h2>
                                <p class="gazette-post-date">{{\Carbon\Carbon::parse($data->published_date)->diffForHumans()}}</p>

                                <div class="blog-post-thumbnail my-5">
                                    <img src="{{($data->thumb) ? $data->thumb : asset('assets/img/default.jpg')}}"
                                         alt="post-thumb">
                                </div>

                                <p>{{\Illuminate\Support\Str::limit(strip_tags($data->short_description), 150)}}</p>

                                <div class="post-continue-reading-share mt-30">
                                    <div class="post-continue-btn">
                                        <a href="{{route('site.singleView',['slug'=>$data->slug])}}" class="font-pt">Continue
                                            Reading <i class="fa fa-chevron-right"
                                                       aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach

            <div class="row">
                <div class="col-12">
                    <div class="gazette-pagination-area">
                        <nav aria-label="Page navigation example">
                            {{ $news_data->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
