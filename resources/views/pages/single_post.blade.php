@extends('layout.main')
@section('title', $news->title)
@section('og-title', $news->title)
@section('og-description', $news->short_description)
@section('og-image', (($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg') )
@section('og-url', asset('').'news/'.$news->slug)


@section('content')
    <div class="row bo-t  bo-b pd-15">

        <div class="col-md-9 bo-r">

            {{--<div class="gazette-post-tag">--}}
            {{--<a href="#">Politices</a>--}}
            {{--</div>--}}
            <h2 class="singlehead">{{$news->title}}</h2>
            <p class="gazette-post-date">{{$news->published_date}}</p>
            <!-- Post Thumbnail -->


            <br>
            @if($news->source == 'admin' AND $news->source_type != 'Video')
                <div class="blog-post-thumbnail ">
                    <img src="{{$news->thumb}}" alt="post-thumb">
                </div>
            @else
                @if($news->source_type == 'Video')
                    <style>
                        .embed-container {
                            position: relative;
                            padding-bottom: 56.25%;
                            height: 0;
                            overflow: hidden;
                            max-width: 100%;
                        }

                        .embed-container iframe, .embed-container object, .embed-container embed {
                            position: absolute;
                            top: 0;
                            left: 0;
                            width: 100%;
                            height: 100%;
                        }
                    </style>
                    <div class='embed-container'>
                        <iframe src='{{$news->original_url}}' frameborder='0' allowfullscreen></iframe>
                    </div>
                @endif
            @endif
            <div class="txtdiv mt-5 ">
                <p class="singletxt">
                    @if($news->source == 'admin' AND $news->source_type == 'Breaking News')
                        {!! $news->long_description !!}
                    @else
                        {!! base64_decode($news->long_description) !!}
                    @endif

                </p>

            </div>
            <div class="post-continue-reading-share d-sm-flex align-items-center justify-content-between mt-30">
                <div class="post-continue-btn">
                </div>
                <div class="post-share-btn-group">
                    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3">


            <div class="">
                <!-- Single Catagory Post -->
                <div class="gazette-single-catagory-post">
                    <div class="single-catagory-post-thumb mb-15">
                        <img src="{{asset('')}}assets/img/blog-img/12.jpg" alt="">
                    </div>
                    <!-- Post Tag -->
                    <div class="gazette-post-tag">
                        <a href="#">Top Stories</a>
                    </div>
                </div>

                ` @foreach($latest_news as $news)
                <!-- Single Catagory Post -->
                    <div class="gazette-single-catagory-post">
                        <h5><a href="{{route('site.singleView',['slug'=>$news->slug])}}"
                               class="font-pt">{{$news->title}}</a></h5>
                        <span>{{$news->published_date}}</span>
                    </div>
                @endforeach
            </div>
            <!-- Advert Widget -->
            <div class=" bo-t">
                <div class="widget-title">
                    <h5>Advert</h5>
                </div>
                <div class="advert-thumb mb-30">
                    <a href="#"><img src="{{asset('')}}assets/img/bg-img/add.png" alt=""></a>
                </div>
            </div>
            <!-- Don't Miss Widget -->
            <div class="donnot-miss-widget">
                <div class="widget-title">
                    <h5>Don't miss</h5>
                </div>
            @foreach($entertainmentNews as $news)
                <!-- Single Don't Miss Post -->
                    <div class="single-dont-miss-post d-flex mb-30">
                        <div class="dont-miss-post-thumb">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="">
                        </div>
                        <div class="dont-miss-post-content">
                            <a href="{{route('site.singleView',['slug'=>$news->slug])}}"
                               class="font-pt">{{$news->title}}</a>
                            <span>{{$news->published_date}}</span>
                        </div>
                    </div>
                @endforeach
            </div>


        </div>
    </div>
@endsection
