@extends('layout.main')
@section('title', 'SiteMap')
@section('content')
    <!-- Breadcumb Area Start -->
    <div class="breadcumb-area section_padding_50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breacumb-content d-flex align-items-center justify-content-between">
                        <h3 class="font-pt mb-0">SiteMap</h3>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area End -->

    <section class="gazette-contact-area pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="gazette-heading">
                        <h4 class="font-bold">Articles by Category</h4>
                    </div>
                    <ul>
                        @foreach($categories as $cat)
                        <li class="pull-left p-4"><a href="{{route('site.sitemapCategory',['slug'=>$cat->slug,'type'=>'category'])}}">{{$cat->name}}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-12 col-md-12">
                    <div class="gazette-heading">
                        <h4 class="font-bold">Articles by Month</h4>
                    </div>
                    <ul>
                        @foreach($dates as $date)
                            <li class="pull-left p-4"><a href="{{route('site.sitemapCategory',['slug'=>$date,'type'=>'date'])}}">{{$date}}</a></li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </section>

@endsection

