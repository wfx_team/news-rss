@extends('layout.main')
@section('title', 'SiteMap')
@section('content')
    <!-- Breadcumb Area Start -->
    <div class="breadcumb-area section_padding_50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breacumb-content d-flex align-items-center justify-content-between">
                        <h3 class="font-pt mb-0">SiteMap</h3>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area End -->

    <section class="gazette-contact-area pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">

                    <ul>
                        @foreach($news as $one)
                        <li class="p-4"><a href="{{route('site.singleView',['slug'=>$one->slug])}}">{{$one->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="gazette-pagination-area">
                        <nav aria-label="Page navigation example">
                            {{ $news->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

