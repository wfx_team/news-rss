@extends('layout.main')

@section('content')
    <div class="breadcumb-area section_padding_50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breacumb-content d-flex align-items-center justify-content-between">

                        <div class="gazette-post-tag">
                            <a href="#">politics</a>
                        </div>
                        <p class="editorial-post-date text-dark mb-0">March 29, 2016</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="gazatte-editorial-area section_padding_100 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="editorial-post-slides owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                 style="transform: translate3d(-6660px, 0px, 0px); transition: all 1s ease 0s; width: 8880px;">
                                <div class="owl-item cloned" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned active" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 1110px; margin-right: 0px;">
                                    <div class="editorial-post-single-slide">
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="editorial-post-thumb">
                                                    <img src="img/blog-img/bitcoin.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="editorial-post-content">

                                                    <div class="gazette-post-tag">
                                                        <a href="#">Editorial</a>
                                                    </div>
                                                    <h2><a href="#" class="font-pt mb-15">Move over, bitcoin. <br>Here
                                                            comes litecoin</a></h2>
                                                    <p class="editorial-post-date mb-15">March 29, 2016</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Suspendisse ultrices egestas nunc, quis venenatis orci tincidunt
                                                        id. Fusce commodo blandit eleifend. Nullam viverra tincidunt
                                                        dolor, at pulvinar dui. Nullam at risus ut ipsum viverra
                                                        posuere. Aliquam quis convallis enim. Nunc pulvinar molestie sem
                                                        id blandit. Nunc venenatis interdum mollis...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-controls">
                            <div class="owl-nav">
                                <div class="owl-prev" style="display: none;">prev</div>
                                <div class="owl-next" style="display: none;">next</div>
                            </div>
                            <div class="owl-dots" style="">
                                <div class="owl-dot active"><span></span></div>
                                <div class="owl-dot"><span></span></div>
                                <div class="owl-dot"><span></span></div>
                                <div class="owl-dot"><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="catagory-welcome-post-area section_padding_100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gazette-pagination-area">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next"><i
                                                class="fa fa-angle-right"></i></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
