@extends('layout.main')
@section('title', 'ScamsBreaking | Home')

@section('og-title', 'ScamsBreaking')
@section('og-description', 'ScamsBreaking.com is an online investigative news portal. Explore us for exclusive and Sensational scams breaking News.')
@section('og-image', asset('').'assets/img/default-img.jpeg')
@section('og-url', asset(''))


@section('content')
    <div class="row bo-t  bo-b pd-15">
        <div class="col-md-3 bo-r">
            <div class="gazette-post-tag">
                <a href="#">Business</a>
            </div>
            @foreach($newsData->businessNews as $news)
                <article class="heading-1 bo-b">
                    <div class="WSJTheme--headline--7VCzo7Ay ">
                        <h3 class="heading-1">
                            <a class="" href="{{route('site.singleView',['slug'=>$news->slug])}}">{{$news->title}}</a>
                        </h3>
                    </div>
                    <p class="txt">
                        {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}
                        <span class="WSJTheme--stats--2HBLhVc9 "></span>
                    </p>
                    <ul class="WSJTheme--bullets--2pLEdfJV ">
                        <li class="WSJTheme--bullet-item--5c1Mqfdr WSJTheme--icon--1lXhFmSW WSJTheme--none--1l-88_52 ">
                        </li>
                    </ul>
                </article>

            @endforeach
            @foreach($newsData->latestNews_body as $news)
                <div class="single-breaking-news-widget">
                    <a class="" href="{{route('site.singleView',['slug'=>$news->slug])}}">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}" alt="">
                        <div class="breakingnews-title">
                            <p>TOP Stories</p>
                        </div>
                        <div class="breaking-news-heading gradient-background-overlay">
                            <h5 class="font-pt">{{$news->title}}</h5>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="col-md-4 bo-r">
            <div class="gazette-post-tag">
                <a href="#">Breaking News</a>
            </div>
            @foreach($newsData->breakingNews as $news)
                <article class="bo-b">
                    <div class="blog-post-thumbnail ">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                             alt="post-thumb">
                    </div>
                    <h3 class="heading-2 ">
                        <a class="" href="{{asset('')}}news/{{$news->slug}}">{{$news->title}}</a>
                    </h3>
                    <p class="txt">
                        {{ \Illuminate\Support\Str::limit(strip_tags(strip_tags($news->short_description),250))}}
                    </p>
                </article>
            @endforeach
        </div>

        <div class="col-md-2">


            <div class="gazette-post-tag">
                <a href="#">Video</a>
            </div>
            @foreach($newsData->video as $news)
                <div class="gazette-single-catagory-post">
                    <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">
                        <div class="single-catagory-post-thumb mb-15">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>

                        <h5>{{$news->title}}
                        </h5>
                        <span> {{$news->published_date}}</span>
                    </a>
                </div>
            @endforeach

            <div class="gazette-post-tag">
                <a href="#">Politics</a>
            </div>
            @foreach($newsData->politicsNews as $news)
                <div class="gazette-single-catagory-post">
                    <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">
{{--                        <div class="single-catagory-post-thumb mb-15">--}}
{{--                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"--}}
{{--                                 alt="{{$news->title}}">--}}
{{--                        </div>--}}

                        <h5>{{$news->title}}
                        </h5>
                        <span> {{$news->published_date}}</span>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="col-md-3">
            <div class="gazette-post-tag">
                <a href="#">india now</a>
            </div>
            @foreach($newsData->aniNews as $news)
                <div class="gazette-single-catagory-post">
                    {{--                    <div class="single-catagory-post-thumb mb-15">--}}
                    {{--                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"--}}
                    {{--                             alt="{{$news->title}}">--}}
                    {{--                    </div>--}}

                    <h5><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                    </h5>
                    <span> {{$news->published_date}}</span>
                </div>
            @endforeach
            <div class="subscribe-widget" id="subscribe">
                <div class="widget-title">
                    <h5>subscribe</h5>
                </div>
                <div class="subscribe-form">
                    @if (\Illuminate\Support\Facades\Session::has('subs_success'))
                        <div class="alert alert-success text">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_success') !!}
                            </small>
                        </div>
                    @endif
                    @if (\Illuminate\Support\Facades\Session::has('subs_error'))
                        <div class="alert alert-danger">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_error') !!}
                            </small>
                        </div>
                    @endif
                    <form action="{{route('site.subscribe')}}" method="post" id="subs_form">
                        @csrf
                        <input type="email" name="subs_email" id="subs_email" placeholder="Your Email"
                               autocomplete="off" required>
                        <button type="submit" id="subs_btn">subscribe</button>
                    </form>
                </div>
            </div>
            <div class="advert-widget">
                <div class="widget-title">
                    <h5>Advert</h5>
                </div>
                <div class="advert-thumb mb-30">
                    <a href="https://tesla.com"><img src="https://scamsbreaking.com/assets/img/bg-img/add.png" alt=""></a>
                </div>
            </div>
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
                <div class="tradingview-widget-container__widget"></div>
                <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/indices/"
                                                             rel="noopener" target="_blank"><span class="blue-text">Indices</span></a>,
                    <a href="https://www.tradingview.com/markets/futures/" rel="noopener" target="_blank"><span
                                class="blue-text">Commodities</span></a> <span class="blue-text">and</span> <a
                            href="https://www.tradingview.com/markets/bonds/" rel="noopener" target="_blank"><span
                                class="blue-text">Bonds</span></a> by TradingView
                </div>
                <script type="text/javascript"
                        src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
                    {
                        "colorTheme"
                    :
                        "light",
                            "dateRange"
                    :
                        "12M",
                            "showChart"
                    :
                        true,
                            "locale"
                    :
                        "en",
                            "largeChartUrl"
                    :
                        "",
                            "isTransparent"
                    :
                        false,
                            "width"
                    :
                        "100%",
                            "height"
                    :
                        "660",
                            "plotLineColorGrowing"
                    :
                        "rgba(11, 83, 148, 1)",
                            "plotLineColorFalling"
                    :
                        "rgba(33, 150, 243, 1)",
                            "gridLineColor"
                    :
                        "rgba(101, 101, 101, 1)",
                            "scaleFontColor"
                    :
                        "rgba(120, 123, 134, 1)",
                            "belowLineFillColorGrowing"
                    :
                        "rgba(33, 150, 243, 0.12)",
                            "belowLineFillColorFalling"
                    :
                        "rgba(33, 150, 243, 0.12)",
                            "symbolActiveColor"
                    :
                        "rgba(33, 150, 243, 0.12)",
                            "tabs"
                    :
                        [
                            {
                                "title": "Indices",
                                "symbols": [
                                    {
                                        "s": "FOREXCOM:SPXUSD",
                                        "d": "S&P 500"
                                    },
                                    {
                                        "s": "FOREXCOM:NSXUSD",
                                        "d": "Nasdaq 100"
                                    },
                                    {
                                        "s": "FOREXCOM:DJI",
                                        "d": "Dow 30"
                                    },
                                    {
                                        "s": "INDEX:NKY",
                                        "d": "Nikkei 225"
                                    },
                                    {
                                        "s": "INDEX:DEU30",
                                        "d": "DAX Index"
                                    },
                                    {
                                        "s": "FOREXCOM:UKXGBP",
                                        "d": "FTSE 100"
                                    }
                                ],
                                "originalTitle": "Indices"
                            },
                            {
                                "title": "Commodities",
                                "symbols": [
                                    {
                                        "s": "CME_MINI:ES1!",
                                        "d": "S&P 500"
                                    },
                                    {
                                        "s": "CME:6E1!",
                                        "d": "Euro"
                                    },
                                    {
                                        "s": "COMEX:GC1!",
                                        "d": "Gold"
                                    },
                                    {
                                        "s": "NYMEX:CL1!",
                                        "d": "Crude Oil"
                                    },
                                    {
                                        "s": "NYMEX:NG1!",
                                        "d": "Natural Gas"
                                    },
                                    {
                                        "s": "CBOT:ZC1!",
                                        "d": "Corn"
                                    }
                                ],
                                "originalTitle": "Commodities"
                            },
                            {
                                "title": "Bonds",
                                "symbols": [
                                    {
                                        "s": "CME:GE1!",
                                        "d": "Eurodollar"
                                    },
                                    {
                                        "s": "CBOT:ZB1!",
                                        "d": "T-Bond"
                                    },
                                    {
                                        "s": "CBOT:UB1!",
                                        "d": "Ultra T-Bond"
                                    },
                                    {
                                        "s": "EUREX:FGBL1!",
                                        "d": "Euro Bund"
                                    },
                                    {
                                        "s": "EUREX:FBTP1!",
                                        "d": "Euro BTP"
                                    },
                                    {
                                        "s": "EUREX:FGBM1!",
                                        "d": "Euro BOBL"
                                    }
                                ],
                                "originalTitle": "Bonds"
                            }
                        ]
                    }
                </script>
            </div>
            <!-- TradingView Widget END -->

            <!-- Breaking News Widget -->


        </div>
    </div>
    <div class="row bo-t  bo-b pd-15">
        <div class="col-md-3 bo-r">
            <div class="gazette-post-tag">
                <a href="#">Health</a>
            </div>

            @foreach($newsData->healthNews as $news)
                <article class="heading-1 bo-b">
                    <div class="WSJTheme--headline--7VCzo7Ay ">
                        <h3 class="heading-1">
                            <a class="" href="{{asset('')}}news/{{$news->slug}}">{{$news->title}}</a></h3>
                    </div>
                    <p class="txt">
                        {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}
                    </p>
                    <ul class="WSJTheme--bullets--2pLEdfJV ">
                        <li class="WSJTheme--bullet-item--5c1Mqfdr WSJTheme--icon--1lXhFmSW WSJTheme--none--1l-88_52 ">
                        </li>
                    </ul>
                </article>

            @endforeach

            <div class="gazette-post-tag">
                <a href="#">sports</a>
            </div>

            @foreach($newsData->sports as $news)
                <div class="gazette-single-catagory-post">
                    <div class="single-catagory-post-thumb mb-15">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                             alt="{{$news->title}}">
                    </div>

                    <h5><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                    </h5>
                    <span> {{$news->published_date}}</span>
                </div>

            @endforeach


        </div>
        <div class="col-md-6 bo-r">
            <div class="header-advert-area pd-15">
                <a href="https://www.ferrari.com/en-EN/auto/f8-spider"><img src="{{asset('')}}assets/img/bg-img/top-advert.png" alt="header-add"></a>
            </div>
            <div class="gazette-todays-post ">
                <div class="gazette-heading">
                    <h4>today’s most popular</h4>
                </div>
                <div class="gazette-post-tag">
                    <a href="#">Life</a>
                </div>
                <!-- Single Today Post -->
                @foreach($newsData->lifeNews as $news)
                    <div class="gazette-single-todays-post d-md-flex align-items-start mb-50">
                        <div class="todays-post-thumb">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>
                        <div class="todays-post-content">

                            <h3><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt mb-2">{{$news->title}}</a>
                            </h3>

                            <p class="txt">{{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}</p>
                        </div>
                    </div>
                @endforeach
            <!-- Single Today Post -->
                <div class="gazette-post-tag">
                    <a href="#">World</a>
                </div>
                @foreach($newsData->worldNews as $news)
                    <article class="bo-b">
                        <div class="blog-post-thumbnail ">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>
                        <h3 class="heading-2 ">
                            <a class="" href="{{asset('')}}news/{{$news->slug}}">{{$news->title}}</a>
                        </h3>
                        <p class="txt">
                            {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}
                        </p>
                    </article>
                @endforeach
            </div>

        </div>


        <div class="col-md-3">
            <div class="gazette-post-tag">
                <a href="#">Science</a>
            </div>
            @foreach($newsData->scienceNews as $news)
                <div class="gazette-single-catagory-post">
                    <div class="single-catagory-post-thumb mb-15">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                             alt="{{$news->title}}">
                    </div>

                    <h5><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                    </h5>
                    <span> {{$news->published_date}}</span>
                </div>
        @endforeach
        <!-- Breaking News Widget -->
        {{--<div class="breaking-news-widget">--}}
        {{--<div class="widget-title">--}}
        {{--<h5>breaking news</h5>--}}
        {{--</div>--}}
        {{--@foreach($newsData->latestNews->all() as $news)--}}
        {{--<div class="single-breaking-news-widget">--}}
        {{--<img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}" alt="">--}}
        {{--<div class="breakingnews-title">--}}
        {{--<p>breaking news</p>--}}
        {{--</div>--}}
        {{--<div class="breaking-news-heading gradient-background-overlay">--}}
        {{--<h5 class="font-pt">{{$news->title}}</h5>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        {{--</div>--}}

        <!-- Don't Miss Widget -->
            <div class="donnot-miss-widget">
                <div class="widget-title">
                    <h5>Don't miss</h5>
                </div>
            @foreach($newsData->entertainmentNews as $news)
                <!-- Single Don't Miss Post -->
                    <div class="single-dont-miss-post d-flex mb-30">
                        <div class="dont-miss-post-thumb">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="">
                        </div>
                        <div class="dont-miss-post-content">
                            <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                            <span>{{$news->published_date}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Advert Widget -->
            <div class="advert-widget">
                <div class="advert-thumb mb-30">
                    <a href="https://tesla.com"><img src="{{asset('')}}assets/img/bg-img/add.png" alt=""></a>
                </div>
            </div>


        </div>
    </div>

@endsection

@section('extra-js')
    <script>

    </script>
@endsection
