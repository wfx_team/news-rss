@extends('layout.main')
@section('title', 'Advertise with Us')
@section('content')
    <section class="gazette-about-us-area ">
        <div class="about-us-content">
            <div class="container">
                <div class="row">

                    <style>
                        .add {
                            position: relative;
                            background-color: black;
                            height: 17vh;
                            min-height: 16rem;
                            width: 100%;
                            overflow: hidden;
                            margin-bottom: 21px;
                        }
                        .add video {
                            position: absolute;
                            top: 50%;
                            left: 50%;
                            min-width: 100%;
                            min-height: 100%;
                            width: auto;
                            height: auto;
                            z-index: 0;
                            -ms-transform: translateX(-50%) translateY(-50%);
                            -moz-transform: translateX(-50%) translateY(-50%);
                            -webkit-transform: translateX(-50%) translateY(-50%);
                            transform: translateX(-50%) translateY(-50%);
                        }

                        .add .container {
                            position: relative;
                            z-index: 2;
                        }

                        .add .overlay {
                            position: absolute;
                            top: 0;
                            left: 0;
                            height: 100%;
                            width: 100%;
                            background-color: black;
                            opacity: 0.5;
                            z-index: 1;
                        }

                        h1, h2, h3, h4, h5, h6 {
                            color: #ffffff;
                            line-height: 1.2;
                            font-family: 'Sora', sans-serif;
                            font-weight: 400;
                        }

                        .leave-comment-btn, .contact-btn {
                            width: 100%;
                            font-size: 18px;
                            height: 50px;
                            border: 2px solid #fff;
                            border-radius: 0;
                            background-color: transparent;
                            z-index: 111;
                            margin-top: -204px;
                        }

                        a, a:hover, a:focus {
                            -webkit-transition: all 500ms ease 0s;
                            transition: all 500ms ease 0s;
                            text-decoration: none;
                            outline: none;
                            color: #fff;
                            font-family: 'Sora', sans-serif;
                            font-weight: 500;
                        }

                        .image-grid-cover {
                            width: 100%;
                            background-size: cover;
                            min-height: 247px;
                            position: relative;
                            margin-bottom: 30px;
                            text-shadow: rgba(0,0,0,.8) 0 1px 0;
                            border-radius: 4px;
                        }
                        .image-grid-clickbox {
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            display: block;
                            width: 100%;
                            height: 100%;
                            z-index: 20;
                            background: rgba(0,0,0,.45);
                        }
                        .cover-wrapper {
                            font-size: 34px;
                            text-align: center;
                            display: block;
                            color: #fff;
                            text-shadow: rgba(0,0,0,.8) 0 1px 0;
                            z-index: 21;
                            position: relative;
                            top: 80px;
                        }
                        a, a:focus, a:hover {
                            text-decoration: none;
                            outline: 0;
                        }
                        .btn-block{
                            font-size: 30px;
                            background: #e00000;
                        }
                    </style>
                </div>
            </div>
            <div class="col-12 col-md-12">
                <header class="add">
                    <div class="overlay"></div>

                    <div class="container h-100">
                        <div class="d-flex text-center h-100">
                            <div class="my-auto w-100 text-white">
                                <h1 class="display-3">ADVERTISE WITH US</h1>
                                <h2> tailor-made solutions for brands and publishers around the world</h2>
                            </div>
                        </div>


                    </div>


                </header>
                <div class="content">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 image-grid-item">
                                <div style="background-image: url(https://images.unsplash.com/photo-1598704066184-80bde4152af4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80);" class="entry-cover image-grid-cover has-image">
                                    <a href="#" class="image-grid-clickbox"></a>
                                    <a href="#" class="cover-wrapper">Advertise your video</a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 image-grid-item">
                                <div style="background-image: url(https://images.unsplash.com/photo-1563986768494-4dee2763ff3f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80	);" class="entry-cover image-grid-cover has-image">
                                    <a href="#" class="image-grid-clickbox"></a>
                                    <a href="#" class="cover-wrapper">Feature your content</a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 image-grid-item">
                                <div style="background-image: url(https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80	);" class="entry-cover image-grid-cover has-image">
                                    <a href="#" class="image-grid-clickbox"></a>
                                    <a href="#" class="cover-wrapper">Editorials </a>
                                </div>
                            </div>
                        </div>

                    <a href="{{asset('')}}contact-us">
                        <button type="button" class="btn btn-secondary btn-lg btn-block">Contact Us Now</button>
                    </a>
                </div>
                <div style="margin-bottom: 100px"></div>


                {{--Open Multiple URLs | Compress Videos | Online PDF Converter--}}
                {{--© 2018 Unminify2.com | Privacy--}}


            </div>

        </div>
        </div>
        </div>


    </section>

@endsection
